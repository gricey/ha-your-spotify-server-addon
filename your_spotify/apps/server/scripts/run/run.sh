#!/bin/sh

source /app/apps/server/scripts/run/deprecated.sh

export API_ENDPOINT="$(bashio::config 'api_endpoint')"
export CLIENT_ENDPOINT="$(bashio::config 'client_endpoint')"
export SPOTIFY_PUBLIC="$(bashio::config 'spotify_public')"
export SPOTIFY_SECRET="$(bashio::config 'spotify_secret')"

cd apps/server && yarn migrate && yarn start
